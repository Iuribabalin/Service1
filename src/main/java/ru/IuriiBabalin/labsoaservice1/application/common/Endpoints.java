package ru.IuriiBabalin.labsoaservice1.application.common;

public interface Endpoints {
    String VEHICLE = "/vehicle";
    String GET_DELETE_VEHICLE = "/vehicle/{vehicleId}";
    String VEHICLE_SEARCH = "/vehicle/search";

    String DELETE_VEHICLE_ENGINE_POWER = "/vehicle/clear";

    String DELETE_VEHICLE_TYPE = "/vehicle/clear/one";

    String UNIQUE_VEHICLE_TYPE = "/vehicle/unique/type";
}
