package ru.IuriiBabalin.labsoaservice1.application.vehicle.model;

public enum VehicleType {
    HELICOPTER,
    DRONE,
    BICYCLE
}
