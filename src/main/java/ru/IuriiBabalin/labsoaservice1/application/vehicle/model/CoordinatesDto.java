package ru.IuriiBabalin.labsoaservice1.application.vehicle.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class CoordinatesDto {
    @Schema(name = "Координата x")
    private long x;
    @Schema(name = "Координата y")
    private Long y;
}
